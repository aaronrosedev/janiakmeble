﻿using CMS.DAL;
using CMS.Foremki;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS
{
    static class Program
    {
        //FTP
        static string ftpAdress = Config.FTPAdress;
        static string ftpLogin = Config.FTPUserName;
        static string ftpPassword = Config.FTPUserPassword;

        const string PhotosFolder = "";

        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
           

            ////List<Dodatki> dodatki = new List<Dodatki>();
            ////dodatki.Add()

            //Meble mebel = new Meble() {
            //    Kategoria = 1,
            //    Nazwa = "Narożnik Pierdziel",
            //    Opis = "Tere fere",
            //    Wymiary = "120x249"
            //};
            ////mebel.Dodatki.Add();


            //string pathSource = @"d:\test.png";
            //string pathNew = @"d:\test2.png";

            //try
            //{

            //    using (FileStream fsSource = new FileStream(pathSource,
            //        FileMode.Open, FileAccess.Read))
            //    {

            //        // Read the source file into a byte array.
            //        byte[] bytes = new byte[fsSource.Length];
            //        int numBytesToRead = (int)fsSource.Length;
            //        int numBytesRead = 0;
            //        while (numBytesToRead > 0)
            //        {
            //            // Read may return anything from 0 to numBytesToRead.
            //            int n = fsSource.Read(bytes, numBytesRead, numBytesToRead);

            //            // Break when the end of the file is reached.
            //            if (n == 0)
            //                break;

            //            numBytesRead += n;
            //            numBytesToRead -= n;
            //        }
            //        numBytesToRead = bytes.Length;

            //        // Write the byte array to the other FileStream.
            //        using (FileStream fsNew = new FileStream(pathNew,
            //            FileMode.Create, FileAccess.Write))
            //        {
            //            fsNew.Write(bytes, 0, numBytesToRead);
            //        }
            //    }
            //}
            //catch (FileNotFoundException ioEx)
            //{
            //     MessageBox.Show(ioEx.Message);
            //}



            ///* Create Object Instance */
            //myFTP ftpClient = new myFTP(ftpAdress, ftpLogin, ftpPassword);

            ///* Upload a File */
            //ftpClient.upload(PhotosFolder + "test.png", @"D:/test.png");

            /////* Download a File */
            ////ftpClient.download("etc/test.txt", @"C:\Users\metastruct\Desktop\test.txt");

            /////* Delete a File */
            ////ftpClient.delete(PhotosFolder + "fromFtpClient/test.txt");

            /////* Rename a File */
            ////ftpClient.rename("etc/test.txt", "test2.txt");

            /////* Create a New Directory */
            ////ftpClient.createDirectory("etc/test");

            /////* Get the Date/Time a File was Created */
            ////string fileDateTime = ftpClient.getFileCreatedDateTime("etc/test.txt");
            //// MessageBox.Show(fileDateTime);

            /////* Get the Size of a File */
            ////string fileSize = ftpClient.getFileSize("etc/test.txt");
            //// MessageBox.Show(fileSize);

            /////* Get Contents of a Directory (Names Only) */
            ////string[] simpleDirectoryListing = ftpClient.directoryListDetailed("/etc");
            ////for (int i = 0; i < simpleDirectoryListing.Count(); i++) {  MessageBox.Show(simpleDirectoryListing[i]); }

            /////* Get Contents of a Directory with Detailed File/Directory Info */
            ////string[] detailDirectoryListing = ftpClient.directoryListDetailed("/etc");
            ////for (int i = 0; i < detailDirectoryListing.Count(); i++) {  MessageBox.Show(detailDirectoryListing[i]); }
            ///* Release Resources */
            //ftpClient = null;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LogIn());


        }
    }
}
