﻿using CMS.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS.Foremki
{
    public partial class DodatekEdit : Form
    {
        private Dodatki D;

        public DodatekEdit()
        {
            InitializeComponent();
            D = new Dodatki();
        }

        public DodatekEdit(int Ref)
        {
            InitializeComponent();
            D = myDAL.GetDodatekById(Ref);
            this.nazwaEdit.Text = D.Nazwa;
            this.zdjecieEdit.ImageLocation = Config.WebsiteAdress + D.Zdjecie;
            this.opisEdit.Text = D.Opis;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //TODO zapis do bazy
            D.Nazwa = this.nazwaEdit.Text;
            D.Opis = this.opisEdit.Text;
            D.Zdjecie = this.zdjecieEdit.ImageLocation;
            if (this.ifDodatek.Checked)
            {
                D.CzyDotatek = 1;
            }
            else if (this.ifStandard.Checked)
            {
                D.CzyDotatek = 0;
            }

            if (myDAL.addOrEditDodatek(D)) 
                this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                string sFileName = choofdlog.FileName;
                //string[] arrAllFiles = choofdlog.FileNames; //used when Multiselect = true
                this.zdjecieEdit.ImageLocation = sFileName;
            }
        }
    }
}
