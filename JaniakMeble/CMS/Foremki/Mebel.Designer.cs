﻿using CMS.Kontrolki;

namespace CMS.Foremki
{
    partial class Mebel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zdjeciaPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.dodatkiPanel = new System.Windows.Forms.CheckedListBox();
            this.opis = new System.Windows.Forms.RichTextBox();
            this.wymiary = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nazwa = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.opisSkrocony = new System.Windows.Forms.RichTextBox();
            this.opisSkroconyIloscZnakow = new System.Windows.Forms.Label();
            this.kategoriaList = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // zdjeciaPanel
            // 
            this.zdjeciaPanel.AutoScroll = true;
            this.zdjeciaPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.zdjeciaPanel.Location = new System.Drawing.Point(622, 49);
            this.zdjeciaPanel.Name = "zdjeciaPanel";
            this.zdjeciaPanel.Size = new System.Drawing.Size(665, 668);
            this.zdjeciaPanel.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(1052, 734);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(235, 58);
            this.button1.TabIndex = 2;
            this.button1.Text = "Zapisz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dodatkiPanel
            // 
            this.dodatkiPanel.FormattingEnabled = true;
            this.dodatkiPanel.Location = new System.Drawing.Point(395, 320);
            this.dodatkiPanel.Name = "dodatkiPanel";
            this.dodatkiPanel.Size = new System.Drawing.Size(221, 394);
            this.dodatkiPanel.TabIndex = 3;
            // 
            // opis
            // 
            this.opis.Location = new System.Drawing.Point(19, 99);
            this.opis.Name = "opis";
            this.opis.Size = new System.Drawing.Size(595, 183);
            this.opis.TabIndex = 4;
            this.opis.Text = "";
            // 
            // wymiary
            // 
            this.wymiary.Location = new System.Drawing.Point(19, 503);
            this.wymiary.Name = "wymiary";
            this.wymiary.Size = new System.Drawing.Size(328, 129);
            this.wymiary.TabIndex = 5;
            this.wymiary.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(618, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Zdjęcia";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(15, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "Opis";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(391, 293);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "Dodatki";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(15, 476);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "Wymiary";
            // 
            // nazwa
            // 
            this.nazwa.Location = new System.Drawing.Point(19, 49);
            this.nazwa.Name = "nazwa";
            this.nazwa.Size = new System.Drawing.Size(344, 20);
            this.nazwa.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(15, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 24);
            this.label5.TabIndex = 11;
            this.label5.Text = "Nazwa";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(12, 734);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(235, 58);
            this.button2.TabIndex = 12;
            this.button2.Text = "Anuluj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(15, 287);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 24);
            this.label6.TabIndex = 14;
            this.label6.Text = "Opis skrócony";
            // 
            // opisSkrocony
            // 
            this.opisSkrocony.BackColor = System.Drawing.SystemColors.Window;
            this.opisSkrocony.Location = new System.Drawing.Point(19, 314);
            this.opisSkrocony.Name = "opisSkrocony";
            this.opisSkrocony.Size = new System.Drawing.Size(328, 129);
            this.opisSkrocony.TabIndex = 13;
            this.opisSkrocony.Text = "";
            this.opisSkrocony.TextChanged += new System.EventHandler(this.opisSkrocony_TextChanged);
            // 
            // opisSkroconyIloscZnakow
            // 
            this.opisSkroconyIloscZnakow.AutoSize = true;
            this.opisSkroconyIloscZnakow.Location = new System.Drawing.Point(312, 295);
            this.opisSkroconyIloscZnakow.Name = "opisSkroconyIloscZnakow";
            this.opisSkroconyIloscZnakow.Size = new System.Drawing.Size(36, 13);
            this.opisSkroconyIloscZnakow.TabIndex = 15;
            this.opisSkroconyIloscZnakow.Text = "0/255";
            // 
            // kategoriaList
            // 
            this.kategoriaList.FormattingEnabled = true;
            this.kategoriaList.Location = new System.Drawing.Point(416, 49);
            this.kategoriaList.Name = "kategoriaList";
            this.kategoriaList.Size = new System.Drawing.Size(154, 21);
            this.kategoriaList.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(412, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 24);
            this.label7.TabIndex = 17;
            this.label7.Text = "Kategoria";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(509, 734);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(235, 58);
            this.button3.TabIndex = 18;
            this.button3.Text = "Usuń ten mebel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Mebel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1299, 804);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.kategoriaList);
            this.Controls.Add(this.opisSkroconyIloscZnakow);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.opisSkrocony);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nazwa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.wymiary);
            this.Controls.Add(this.opis);
            this.Controls.Add(this.dodatkiPanel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.zdjeciaPanel);
            this.Name = "Mebel";
            this.Text = "Mebel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel zdjeciaPanel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckedListBox dodatkiPanel;
        private System.Windows.Forms.RichTextBox opis;
        private System.Windows.Forms.RichTextBox wymiary;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox nazwa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox opisSkrocony;
        private System.Windows.Forms.Label opisSkroconyIloscZnakow;
        private System.Windows.Forms.ComboBox kategoriaList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button3;
    }
}