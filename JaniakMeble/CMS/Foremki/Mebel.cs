﻿using CMS.DAL;
using CMS.Kontrolki;
//using JaniakMeble.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS.Foremki
{
    public partial class Mebel : Form
    {
        private int Ref;

        public Mebel(int Ref)
        {
            this.Ref = Ref;
            InitializeComponent();
            this.zdjeciaPanel.Controls.Add(new DodajZdjecie());

            //kategorie
            foreach (var A in myDAL.GetListaKategorii())
                this.kategoriaList.Items.Add(A);
            //dodatki
            foreach (var A in myDAL.GetListaDodatkow())
                this.dodatkiPanel.Items.Add(A);

            CMS.DAL.Meble M = myDAL.GetMebelById(Ref);

            this.opis.Text = M.Opis;
            this.opisSkrocony.Text = M.OpisKrotki;
            this.kategoriaList.SelectedItem = M.Kategorie.Nazwa;
            this.nazwa.Text = M.Nazwa;
            this.wymiary.Text = M.Wymiary;

            foreach(var A in M.ZdjeciaMebli)
            {
                this.zdjeciaPanel.Controls.Add(new Kontrolki.Zdjecia(A.Numer.ToString(), A.Link));
            }

            foreach (var A in M.Dodatki)
            {
                int index = this.dodatkiPanel.Items.IndexOf(A.Nazwa);
                this.dodatkiPanel.SetItemChecked(index,true);
            }


        }

        //public int GetMaxNumber()
        //{
        //    this.Controls.OfType<Zdjecia>()
        //}

        public Mebel()
        {
            InitializeComponent();
            this.zdjeciaPanel.Controls.Add(new DodajZdjecie());
            //kategorie
            foreach (var A in myDAL.GetListaKategorii())
                this.kategoriaList.Items.Add(A);
            //dodatki
            foreach (var A in myDAL.GetListaDodatkow())
                this.dodatkiPanel.Items.Add(A);
        }

        private void opisSkrocony_TextChanged(object sender, EventArgs e)
        {
            //Walidacja skroconego opisu
            if(opisSkrocony.Text.Length>255) this.BackColor = System.Drawing.SystemColors.Highlight;
            else this.BackColor = System.Drawing.SystemColors.Control;
            this.opisSkroconyIloscZnakow.Text = opisSkrocony.Text.Length.ToString() + "/255";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Opisy
            Meble M = new Meble()
            {
                Ref = this.Ref,
                Nazwa = this.nazwa.Text,
                Opis = this.opis.Text,
                OpisKrotki = this.opisSkrocony.Text,                
                Wymiary = this.wymiary.Text
            };

            // Pobranie kategorii
            M.Kategoria = myDAL.GetKategoriaByName(this.kategoriaList.SelectedItem.ToString()).Ref;
            

            //Dodatki
            List<string> ListaDodatkowString = new List<string>();
            foreach(var A in this.dodatkiPanel.CheckedItems)
            {
                ListaDodatkowString.Add(A.ToString());
            }
            M.Dodatki = myDAL.GetDodatekByName(ListaDodatkowString);

            //Zdjecia            
            foreach (var A in this.zdjeciaPanel.Controls.OfType<Kontrolki.Zdjecia>())
            {                
                int numerZdjecia = int.Parse(A.numerZdjecia);
                M.ZdjeciaMebli.Add(new ZdjeciaMebli() { Link = A.adresZdjecia, Numer = numerZdjecia });
            }
            try
            {
                myDAL.addOrEditMebel(M);
                this.Close();
            }
            catch(Exception ex) {
                MessageBox.Show("Błąd podczas dodawania mebla" + ex);
            }
               

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Czy na pewno chcesz usunąć ten mebel", "Usuwanie", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                myDAL.deleteMebel(Ref);
                this.Close();
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }
    }
}
