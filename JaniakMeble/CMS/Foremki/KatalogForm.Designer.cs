﻿namespace CMS.Foremki
{
    partial class KatalogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpisKatalog = new System.Windows.Forms.RichTextBox();
            this.NazwaKatalogu = new System.Windows.Forms.TextBox();
            this.zdjeciaPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.kategorieCombo = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.threeStars = new System.Windows.Forms.RadioButton();
            this.twoStars = new System.Windows.Forms.RadioButton();
            this.oneStar = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OpisKatalog
            // 
            this.OpisKatalog.Location = new System.Drawing.Point(12, 315);
            this.OpisKatalog.Name = "OpisKatalog";
            this.OpisKatalog.Size = new System.Drawing.Size(285, 211);
            this.OpisKatalog.TabIndex = 0;
            this.OpisKatalog.Text = "";
            // 
            // NazwaKatalogu
            // 
            this.NazwaKatalogu.Location = new System.Drawing.Point(15, 46);
            this.NazwaKatalogu.Name = "NazwaKatalogu";
            this.NazwaKatalogu.Size = new System.Drawing.Size(282, 20);
            this.NazwaKatalogu.TabIndex = 3;
            // 
            // zdjeciaPanel
            // 
            this.zdjeciaPanel.AutoScroll = true;
            this.zdjeciaPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.zdjeciaPanel.Location = new System.Drawing.Point(328, 46);
            this.zdjeciaPanel.Name = "zdjeciaPanel";
            this.zdjeciaPanel.Size = new System.Drawing.Size(650, 480);
            this.zdjeciaPanel.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(832, 606);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 58);
            this.button1.TabIndex = 5;
            this.button1.Text = "Zapisz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 606);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 58);
            this.button2.TabIndex = 6;
            this.button2.Text = "Anuluj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(324, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 24);
            this.label3.TabIndex = 7;
            this.label3.Text = "Zdjęcia";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(11, 288);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Opis (opcjonalny)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(11, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "Nazwa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "Kategoria";
            // 
            // kategorieCombo
            // 
            this.kategorieCombo.FormattingEnabled = true;
            this.kategorieCombo.Location = new System.Drawing.Point(16, 118);
            this.kategorieCombo.Name = "kategorieCombo";
            this.kategorieCombo.Size = new System.Drawing.Size(281, 21);
            this.kategorieCombo.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.threeStars);
            this.panel1.Controls.Add(this.twoStars);
            this.panel1.Controls.Add(this.oneStar);
            this.panel1.Location = new System.Drawing.Point(16, 161);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(281, 100);
            this.panel1.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(-1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 24);
            this.label5.TabIndex = 13;
            this.label5.Text = "Gwiazdki";
            // 
            // threeStars
            // 
            this.threeStars.AutoSize = true;
            this.threeStars.Location = new System.Drawing.Point(3, 79);
            this.threeStars.Name = "threeStars";
            this.threeStars.Size = new System.Drawing.Size(43, 17);
            this.threeStars.TabIndex = 2;
            this.threeStars.TabStop = true;
            this.threeStars.Text = "* * *";
            this.threeStars.UseVisualStyleBackColor = true;
            // 
            // twoStars
            // 
            this.twoStars.AutoSize = true;
            this.twoStars.Location = new System.Drawing.Point(3, 56);
            this.twoStars.Name = "twoStars";
            this.twoStars.Size = new System.Drawing.Size(36, 17);
            this.twoStars.TabIndex = 1;
            this.twoStars.TabStop = true;
            this.twoStars.Text = "* *";
            this.twoStars.UseVisualStyleBackColor = true;
            // 
            // oneStar
            // 
            this.oneStar.AutoSize = true;
            this.oneStar.Location = new System.Drawing.Point(3, 33);
            this.oneStar.Name = "oneStar";
            this.oneStar.Size = new System.Drawing.Size(29, 17);
            this.oneStar.TabIndex = 0;
            this.oneStar.TabStop = true;
            this.oneStar.Text = "*";
            this.oneStar.UseVisualStyleBackColor = true;
            // 
            // KatalogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 676);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.kategorieCombo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.zdjeciaPanel);
            this.Controls.Add(this.NazwaKatalogu);
            this.Controls.Add(this.OpisKatalog);
            this.Name = "KatalogForm";
            this.Text = "KatalogForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox OpisKatalog;
        private System.Windows.Forms.TextBox NazwaKatalogu;
        private System.Windows.Forms.FlowLayoutPanel zdjeciaPanel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox kategorieCombo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton threeStars;
        private System.Windows.Forms.RadioButton twoStars;
        private System.Windows.Forms.RadioButton oneStar;
    }
}