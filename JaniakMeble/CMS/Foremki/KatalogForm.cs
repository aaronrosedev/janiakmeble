﻿using CMS.DAL;
using CMS.Kontrolki;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS.Foremki
{
    public partial class KatalogForm : Form
    {
        private int Ref;

        public KatalogForm()
        {
            InitializeComponent();
            this.zdjeciaPanel.Controls.Add(new DodajZdjecie());

            //kategorie
            foreach (var A in myDAL.GetListaKategoriiKatalog())
                this.kategorieCombo.Items.Add(A.Nazwa);
        }

        public KatalogForm(int id)
        {
            this.Ref = id;
            //pobieranie katalogu
            InitializeComponent();

            this.zdjeciaPanel.Controls.Add(new DodajZdjecie());

            var katalog = myDAL.GetKatalogById(id);

            //kategorie
            foreach (var A in myDAL.GetListaKategoriiKatalog())
                this.kategorieCombo.Items.Add(A.Nazwa);

            //materialy            
            foreach (var A in katalog.katalog_zdjecia)
                this.zdjeciaPanel.Controls.Add(new Kontrolki.Zdjecia(A.Numer_tkaniny, A.Link));

            //nazwa
            this.NazwaKatalogu.Text = katalog.Nazwa;
            this.OpisKatalog.Text = katalog.Opis;

            switch (katalog.Gwiazdki)
            {
                case 1: {
                        this.oneStar.Checked = true;
                        break;
                    }
            case 2:
                {
                    this.twoStars.Checked = true;
                    break;
                }
            case 3:
                {
                    this.threeStars.Checked = true;
                    break;
                }
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {

            int stars;
            if (this.oneStar.Checked) stars = 1;
            else if (this.twoStars.Checked) stars = 2;
            else if (this.threeStars.Checked) stars = 3;
            else stars = 0;
            //Opisy
            DAL.Katalog M = new DAL.Katalog()
            {
                Ref = this.Ref,
                Nazwa = this.NazwaKatalogu.Text,
                Opis = this.OpisKatalog.Text,
                Gwiazdki = stars
            };

            // Pobranie kategorii
            M.Kategoria = myDAL.GetKategoriaByNameKatalog(this.kategorieCombo.SelectedItem.ToString()).Id;


          
            //Zdjecia            
            foreach (var A in this.zdjeciaPanel.Controls.OfType<Kontrolki.Zdjecia>())
            {
                M.katalog_zdjecia.Add(new katalog_zdjecia() { Link = A.adresZdjecia, Numer_tkaniny = A.numerZdjecia });
            }
            try
            {
                myDAL.addOrEditKatalog(M);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas dodawania mebla" + ex);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
