﻿using CMS.DAL;
using CMS.Kontrolki;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CMS.Foremki
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            List<Mebelek> ListaMebelkow = new List<Mebelek>();
            List<Dodatek> ListaDodatkow = new List<Dodatek>();
            List<Kontrolki.Katalog> ListaKatalogow = new List<Kontrolki.Katalog>();

            this.MeblePanel.Controls.Add(new NowyMebelek());
            this.DodatkiPanel.Controls.Add(new NowyDodatek());
            this.KatalogPanel.Controls.Add(new NowyKatalog());

            
            using (var db = new JMDB())
            {
                //meble
                var lista = (from M in db.Meble
                            orderby M.Nazwa
                            select M).ToList();
                foreach(var A in lista)
                {
                    Mebelek M = new Mebelek(A.Nazwa, Config.WebsiteAdress + A.ZdjeciaMebli.OrderBy(n=>n.Numer).First().Link, A.Ref);
                    ListaMebelkow.Add(M);
                }

                //dodatki
                var lista2 = (from D in db.Dodatki
                             orderby D.Nazwa
                             select D).ToList();
                foreach (var A in lista2)
                {
                    Dodatek D = new Dodatek(A.Nazwa, Config.WebsiteAdress + A.Zdjecie, A.Ref);
                    ListaDodatkow.Add(D);
                }

                //katalogi
                var lista3 = (from K in db.Katalog
                              orderby K.Nazwa
                              select K).ToList();
                foreach (var A in lista3)
                {
                    Kontrolki.Katalog D = new Kontrolki.Katalog(A.Nazwa,A.katalog_kategorie.Nazwa , Config.WebsiteAdress + A.katalog_zdjecia.First().Link, A.Ref);
                    ListaKatalogow.Add(D);
                }
            }

            foreach(var M in ListaMebelkow)
                this.MeblePanel.Controls.Add(M);
            foreach (var D in ListaDodatkow)
                this.DodatkiPanel.Controls.Add(D);
            foreach (var K in ListaKatalogow)
                this.KatalogPanel.Controls.Add(K);
        }

        private void tabPage1_ControlAdded(object sender, ControlEventArgs e)
        {
            this.Refresh();
        }
    }
}
