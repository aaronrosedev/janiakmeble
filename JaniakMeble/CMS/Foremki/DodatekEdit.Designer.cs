﻿namespace CMS.Foremki
{
    partial class DodatekEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zdjecieEdit = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nazwaEdit = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.opisEdit = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.ifDodatek = new System.Windows.Forms.RadioButton();
            this.ifStandard = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.zdjecieEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // zdjecieEdit
            // 
            this.zdjecieEdit.Location = new System.Drawing.Point(52, 53);
            this.zdjecieEdit.Name = "zdjecieEdit";
            this.zdjecieEdit.Size = new System.Drawing.Size(282, 248);
            this.zdjecieEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.zdjecieEdit.TabIndex = 0;
            this.zdjecieEdit.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(48, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nazwa";
            // 
            // nazwaEdit
            // 
            this.nazwaEdit.Location = new System.Drawing.Point(120, 12);
            this.nazwaEdit.Name = "nazwaEdit";
            this.nazwaEdit.Size = new System.Drawing.Size(214, 20);
            this.nazwaEdit.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(52, 307);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(282, 24);
            this.button1.TabIndex = 3;
            this.button1.Text = "Wybierz zdjęcie";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // opisEdit
            // 
            this.opisEdit.Location = new System.Drawing.Point(52, 375);
            this.opisEdit.Name = "opisEdit";
            this.opisEdit.Size = new System.Drawing.Size(282, 182);
            this.opisEdit.TabIndex = 4;
            this.opisEdit.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(48, 348);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "Opis";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(218, 633);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 60);
            this.button2.TabIndex = 6;
            this.button2.Text = "Zapisz";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(52, 633);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(116, 60);
            this.button3.TabIndex = 7;
            this.button3.Text = "Anuluj";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ifDodatek
            // 
            this.ifDodatek.AutoSize = true;
            this.ifDodatek.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ifDodatek.Location = new System.Drawing.Point(52, 562);
            this.ifDodatek.Name = "ifDodatek";
            this.ifDodatek.Size = new System.Drawing.Size(88, 24);
            this.ifDodatek.TabIndex = 8;
            this.ifDodatek.TabStop = true;
            this.ifDodatek.Text = "Dodatek";
            this.ifDodatek.UseVisualStyleBackColor = true;
            // 
            // ifStandard
            // 
            this.ifStandard.AutoSize = true;
            this.ifStandard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ifStandard.Location = new System.Drawing.Point(52, 592);
            this.ifStandard.Name = "ifStandard";
            this.ifStandard.Size = new System.Drawing.Size(93, 24);
            this.ifStandard.TabIndex = 9;
            this.ifStandard.TabStop = true;
            this.ifStandard.Text = "Standard";
            this.ifStandard.UseVisualStyleBackColor = true;
            // 
            // DodatekEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 718);
            this.Controls.Add(this.ifStandard);
            this.Controls.Add(this.ifDodatek);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.opisEdit);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nazwaEdit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.zdjecieEdit);
            this.Name = "DodatekEdit";
            this.Text = "DodatekEdit";
            ((System.ComponentModel.ISupportInitialize)(this.zdjecieEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox zdjecieEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nazwaEdit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox opisEdit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RadioButton ifDodatek;
        private System.Windows.Forms.RadioButton ifStandard;
    }
}