﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS.Kontrolki
{
    public partial class Katalog : UserControl
    {
       
        public int Ref;
        public string Url;
        public string Nazwa;
        public string Kategoria;

        public Katalog(string _Nazwa, string _Kategoria, string _Url, int _Ref)
        {
            Ref = _Ref;
            Nazwa = _Nazwa;
            Url = _Url;
            Kategoria = _Kategoria;
            InitializeComponent();
            this.NazwaKatalogu.Text = Nazwa;
            this.photo.ImageLocation = Url;
            this.KategoriaKatalogu.Text = Kategoria;
        }

        private void button_Click(object sender, EventArgs e)
        {
            CMS.Foremki.KatalogForm M = new Foremki.KatalogForm(Ref);
            M.Show();
        }
    }
}
