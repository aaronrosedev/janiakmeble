﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CMS.Foremki;

namespace CMS.Kontrolki
{
    public partial class NowyDodatek : UserControl
    {
        public NowyDodatek()
        {
            InitializeComponent();
        }

        private void NowyDodatekBtn_Click(object sender, EventArgs e)
        {
            DodatekEdit D = new DodatekEdit();
            D.Show();
        }
    }
}
