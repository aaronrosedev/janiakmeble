﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CMS.Foremki;

namespace CMS.Kontrolki
{
    public partial class NowyKatalog : UserControl
    {
        public NowyKatalog()
        {
            InitializeComponent();
        }

        private void NowyKatalogBtn_Click(object sender, EventArgs e)
        {
            KatalogForm K = new KatalogForm();
            K.Show();
        }
    }
}
