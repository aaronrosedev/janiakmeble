﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS.Kontrolki
{
    public partial class DodajZdjecie : UserControl
    {
        public DodajZdjecie()
        {
            InitializeComponent();            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = true;
            List<string> arrAllFiles = new List<string>();

            if (choofdlog.ShowDialog() == DialogResult.OK)             
                arrAllFiles = choofdlog.FileNames.ToList();                
            
            if (arrAllFiles.Count > 0)
            {
                foreach (var A in arrAllFiles)
                {
                    //próba znalezienia najwyższego numeru zdjęcia i przypisania nowego zdjęcia z jeszcze większym numerem
                    
                    int max = 0;
                    try
                    {
                        foreach (var B in Parent.Controls.OfType<Zdjecia>())
                        {
                            int numer = int.Parse(B.numerZdjecia);
                            if (numer > max) max = numer;
                        }
                    }
                    catch
                    {
                        max = -1;
                    }
                    finally
                    {
                        string nazwaZdjecia = (max + 1).ToString();
                        if (Parent.Parent.Name.Equals("KatalogForm"))
                        {
                            nazwaZdjecia = A.Replace("\\","/").Split('/').Last().ToUpper().Replace(".JPG", "").Replace(".PNG","").Split('-').Last();           
                        }
                        Parent.Controls.Add(new Zdjecia(nazwaZdjecia, A));
                        Parent.Refresh();
                    }
                }
            }
        }
    }
}
