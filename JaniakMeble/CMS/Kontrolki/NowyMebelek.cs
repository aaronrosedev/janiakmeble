﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CMS.Foremki;

namespace CMS.Kontrolki
{
    public partial class NowyMebelek : UserControl
    {
        public NowyMebelek()
        {
            InitializeComponent();
        }

        private void NowyMebelekBtn_Click(object sender, EventArgs e)
        {
            Mebel m = new Mebel();
            m.Show();
        }
    }
}
