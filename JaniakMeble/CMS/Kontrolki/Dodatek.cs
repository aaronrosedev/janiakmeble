﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS.Kontrolki
{
    public partial class Dodatek : UserControl
    {
        public int Ref;
        public string Url;
        public string Nazwa;

        public Dodatek(string _Nazwa, string _Url, int _Ref)
        {
            Ref = _Ref;
            Nazwa = _Nazwa;
            Url = _Url;
            InitializeComponent();
            this.NazwaDodatku.Text = Nazwa;
            this.photo.ImageLocation = Url;
        }


        private void button_Click(object sender, EventArgs e)
        {
            CMS.Foremki.DodatekEdit M = new Foremki.DodatekEdit(Ref);
            M.Show();
        }
    }
}
