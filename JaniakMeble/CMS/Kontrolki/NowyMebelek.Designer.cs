﻿namespace CMS.Kontrolki
{
    partial class NowyMebelek
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.NowyMebelekBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NowyMebelekBtn
            // 
            this.NowyMebelekBtn.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.NowyMebelekBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NowyMebelekBtn.Font = new System.Drawing.Font("Myriad Hebrew", 26.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NowyMebelekBtn.ForeColor = System.Drawing.Color.YellowGreen;
            this.NowyMebelekBtn.Location = new System.Drawing.Point(3, 3);
            this.NowyMebelekBtn.Name = "NowyMebelekBtn";
            this.NowyMebelekBtn.Size = new System.Drawing.Size(189, 204);
            this.NowyMebelekBtn.TabIndex = 0;
            this.NowyMebelekBtn.Text = "Dodaj nowy";
            this.NowyMebelekBtn.UseVisualStyleBackColor = true;
            this.NowyMebelekBtn.Click += new System.EventHandler(this.NowyMebelekBtn_Click);
            // 
            // NowyMebelek
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.NowyMebelekBtn);
            this.Name = "NowyMebelek";
            this.Size = new System.Drawing.Size(195, 210);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button NowyMebelekBtn;
    }
}
