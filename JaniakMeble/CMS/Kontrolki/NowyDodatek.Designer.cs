﻿namespace CMS.Kontrolki
{
    partial class NowyDodatek
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.NowyDodatekBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NowyDodatekBtn
            // 
            this.NowyDodatekBtn.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.NowyDodatekBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NowyDodatekBtn.Font = new System.Drawing.Font("Myriad Hebrew", 26.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NowyDodatekBtn.ForeColor = System.Drawing.Color.YellowGreen;
            this.NowyDodatekBtn.Location = new System.Drawing.Point(3, 3);
            this.NowyDodatekBtn.Name = "NowyDodatekBtn";
            this.NowyDodatekBtn.Size = new System.Drawing.Size(189, 204);
            this.NowyDodatekBtn.TabIndex = 1;
            this.NowyDodatekBtn.Text = "Dodaj nowy";
            this.NowyDodatekBtn.UseVisualStyleBackColor = true;
            this.NowyDodatekBtn.Click += new System.EventHandler(this.NowyDodatekBtn_Click);
            // 
            // NowyDodatek
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.NowyDodatekBtn);
            this.Name = "NowyDodatek";
            this.Size = new System.Drawing.Size(195, 210);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button NowyDodatekBtn;
    }
}
