﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS.Kontrolki
{
    public partial class Zdjecia : UserControl
    {
        public string numerZdjecia;
        public string adresZdjecia;
        public Color kolor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="N">Numer zdjęcia</param>
        /// <param name="A">Link do zdjęcia</param>
        public Zdjecia(string N, string A)
        {
            //if (N == null)
            //{
            //    int? Max = 0;
            //    foreach (Zdjecia zdjecie in Parent.Controls.OfType<Zdjecia>())
            //    {
            //        if (zdjecie.numerZdjecia > Max) Max = zdjecie.numerZdjecia;
            //    }
            //    N = Max + 1;
            //}
            numerZdjecia = N;
            adresZdjecia = A;            
            InitializeComponent();

            this.Image.ImageLocation = adresZdjecia.StartsWith(Config.PhotosFolder) ? Config.WebsiteAdress + adresZdjecia : adresZdjecia;
            this.number.Text = numerZdjecia.ToString();            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Show the color dialog.
            DialogResult result = colorDialog1.ShowDialog();
            // See if user pressed ok.
            if (result == DialogResult.OK)
            {
                // Set form background to the selected color.
                this.BackColor = colorDialog1.Color;
            }
        }

        private void number_TextChanged(object sender, EventArgs e)
        {
            numerZdjecia = this.number.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Parent.Controls.Remove(this);
        }
    }
}
