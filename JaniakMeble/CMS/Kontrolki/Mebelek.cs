﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMS.Kontrolki
{
    public partial class Mebelek : UserControl
    {
        public int Ref;
        public string Url;
        public string Nazwa;

        public Mebelek(string _Nazwa, string _Url, int _Ref)
        {
            Ref = _Ref;
            Nazwa = _Nazwa;
            Url = _Url;
            InitializeComponent();
            this.NazwaMebelka.Text = Nazwa;
            this.photo.ImageLocation = Url;
        }

        private void button_Click(object sender, EventArgs e)
        {
            CMS.Foremki.Mebel M = new Foremki.Mebel(Ref);
            M.Show();
        }
    }
}
