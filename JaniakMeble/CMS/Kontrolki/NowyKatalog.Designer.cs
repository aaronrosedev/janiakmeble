﻿namespace CMS.Kontrolki
{
    partial class NowyKatalog
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.NowyKatalogBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NowyKatalogBtn
            // 
            this.NowyKatalogBtn.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.NowyKatalogBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NowyKatalogBtn.Font = new System.Drawing.Font("Myriad Hebrew", 26.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NowyKatalogBtn.ForeColor = System.Drawing.Color.YellowGreen;
            this.NowyKatalogBtn.Location = new System.Drawing.Point(3, 3);
            this.NowyKatalogBtn.Name = "NowyKatalogBtn";
            this.NowyKatalogBtn.Size = new System.Drawing.Size(189, 204);
            this.NowyKatalogBtn.TabIndex = 2;
            this.NowyKatalogBtn.Text = "Dodaj nowy";
            this.NowyKatalogBtn.UseVisualStyleBackColor = true;
            this.NowyKatalogBtn.Click += new System.EventHandler(this.NowyKatalogBtn_Click);
            // 
            // NowyKatalog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.NowyKatalogBtn);
            this.Name = "NowyKatalog";
            this.Size = new System.Drawing.Size(195, 210);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button NowyKatalogBtn;
    }
}
