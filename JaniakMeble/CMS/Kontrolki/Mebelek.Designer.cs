﻿using System;

namespace CMS.Kontrolki
{
    partial class Mebelek
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.photo = new System.Windows.Forms.PictureBox();
            this.button = new System.Windows.Forms.Button();
            this.NazwaMebelka = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.photo)).BeginInit();
            this.SuspendLayout();
            // 
            // photo
            // 
            this.photo.ImageLocation = "adses";
            this.photo.Location = new System.Drawing.Point(12, 48);
            this.photo.Name = "photo";
            this.photo.Size = new System.Drawing.Size(164, 114);
            this.photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.photo.TabIndex = 0;
            this.photo.TabStop = false;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(12, 178);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(164, 23);
            this.button.TabIndex = 1;
            this.button.Text = "Edytuj";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // NazwaMebelka
            // 
            this.NazwaMebelka.AutoSize = true;
            this.NazwaMebelka.Location = new System.Drawing.Point(9, 10);
            this.NazwaMebelka.Name = "NazwaMebelka";
            this.NazwaMebelka.Size = new System.Drawing.Size(40, 13);
            this.NazwaMebelka.TabIndex = 2;
            this.NazwaMebelka.Text = "Nazwa";
            // 
            // Mebelek
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.NazwaMebelka);
            this.Controls.Add(this.button);
            this.Controls.Add(this.photo);
            this.Name = "Mebelek";
            this.Size = new System.Drawing.Size(193, 208);
            ((System.ComponentModel.ISupportInitialize)(this.photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox photo;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.Label NazwaMebelka;
    }
}
