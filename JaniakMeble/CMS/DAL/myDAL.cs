﻿using CMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Windows.Forms;

namespace CMS.DAL
{
    public class myDAL
    {
        public static void addPhoto(string Url)
        {

        }

        public static void addMebel(Meble M)
        {
            using(var db = new JMDB())
            {
                db.Meble.Add(M);
                db.SaveChanges();
            }
        }

        public static bool addOrEditDodatek(Dodatki N)
        {          
            try
            {
                using (var dbMebel = new JMDB())
                {
                    var lista = (from D in dbMebel.Dodatki
                                 where D.Ref == N.Ref
                                 select D).FirstOrDefault();

                    if (!N.Zdjecie.Equals("Resources"))
                    {

                        N.Zdjecie = N.Zdjecie.Replace("\\", "/");
                        string FileName = N.Zdjecie.Split('/').Last();

                        /* Create Object Instance */
                        myFTP ftpClient = new myFTP(Config.FTPAdress, Config.FTPUserName, Config.FTPUserPassword);

                        /* Upload a File */
                        string Route = Config.PhotosFolder + "/" + FileName;
                        ftpClient.upload(Route, N.Zdjecie);
                        N.Zdjecie = Route;
                    }

                    if (lista != null)
                    {
                        lista = N;
                        dbMebel.SaveChanges();
                    }
                    else
                    {
                        dbMebel.Dodatki.Add(N);
                        dbMebel.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił bład podczas dodawania dodatku! :( " + ex);
                return false;
            }
            return true;
        }

        internal static List<katalog_kategorie> GetListaKategoriiKatalog()
        {
            List<katalog_kategorie> kat;
            using (var db = new JMDB())
            {
                kat = (from K in db.katalog_kategorie
                      select K).ToList();
            }
            return kat;
        }

        public static void addOrEditMebel(Meble M)
        {
            try
            {
                using (var db = new JMDB())
                {
                    var dbMebel = db.Meble.FirstOrDefault(x => x.Ref == M.Ref);

                    // wysyłanie zdjęć na serwer
                    foreach (var N in M.ZdjeciaMebli)
                    {

                        if (!N.Link.StartsWith(Config.PhotosFolder))
                        {

                            N.Link = N.Link.Replace("\\", "/");
                            string FileName = N.Link.Split('/').Last();

                            /* Create Object Instance */
                            myFTP ftpClient = new myFTP(Config.FTPAdress, Config.FTPUserName, Config.FTPUserPassword);

                            /* Upload a File */
                            string Route = string.Format("{0}/{1}", Config.PhotosFolder,FileName);
                            ftpClient.upload(Route, N.Link);

                            //Aktualizacja ścieżki
                            N.Link = Route;
                            
                        }
                    }


                    //Załadowanie dodatków z bazy danych i przypisanie tej referencj do nowej pozycj mebla  
                    List<Dodatki> listaDodatkow = new List<Dodatki>();   

                    //for (int i = 0; i < M.Dodatki.Count(); i++) {
                    //    M.Dodatki.ToArray()[i] = db.Dodatki.FirstOrDefault(x => x.Ref == M.Dodatki.ElementAt(i).Ref);
                    //}

                    foreach (var A in M.Dodatki)
                    {
                        //var B = (from D in db.Dodatki
                        //         where D.Ref == A.Ref
                        //         select D).FirstOrDefault();

                        listaDodatkow.Add(db.Dodatki.FirstOrDefault(x => x.Ref == A.Ref));
                        //A = db.Dodatki.FirstOrDefault(x => x.Ref = A.Ref);
                    }
                    M.Dodatki.Clear();
                    M.Dodatki = listaDodatkow;

                    if (dbMebel != null)
                    {
                        db.Entry(dbMebel).CurrentValues.SetValues(M);
                        //db.Entry(dbMebel.ZdjeciaMebli).CurrentValues.SetValues(M.ZdjeciaMebli);
                        //db.Entry(dbMebel.Dodatki).CurrentValues.SetValues(M.Dodatki);
                        //dbMebel = M;
                        db.SaveChanges();
                    }
                    else
                    {
                        db.Meble.Add(M);
                        db.SaveChanges();
                    }
                }
            }
            catch(Exception ex)
            {
                if (ex.InnerException.InnerException.Message != null)
                    MessageBox.Show("Wystąpił bład podczas dodawania dodatku! :(  " + ex.InnerException.InnerException.Message);
                else
                    MessageBox.Show("Wystąpił bład podczas dodawania dodatku! :(  " + ex);
            }
        }

        public static void deleteMebel(int id)
        {
            using (var db = new JMDB())
            {
                var dbMebel = (from Ma in db.Meble
                               where Ma.Ref == id
                               select Ma).FirstOrDefault();

                db.Meble.Remove(dbMebel);
                try
                {
                    db.SaveChanges();
                }
                catch(Exception e)
                {
                    MessageBox.Show("Problem z usunięciem rekordu z bazy danych: " + e.InnerException);
                }
            }

        }

            public static void addOrEditKatalog(Katalog K)
        {
            try
            {
                using (var db = new JMDB())
                {
                    var dbKatalog = (from Kk in db.Katalog
                                   where Kk.Ref == K.Ref
                                   select Kk).FirstOrDefault();

                    foreach (var N in K.katalog_zdjecia)
                    {

                        //Dodawanie zdjęć
                        if (!N.Link.StartsWith(Config.PhotosFolder))
                        {

                            N.Link = N.Link.Replace("\\", "/");
                            string FileName = N.Link.Split('/').Last();

                            /* Create Object Instance */
                            myFTP ftpClient = new myFTP(Config.FTPAdress, Config.FTPUserName, Config.FTPUserPassword);

                            /* Upload a File */
                            string Route = string.Format("{0}/{1}", Config.PhotosFolder, FileName);
                            ftpClient.upload(Route, N.Link);

                            //Aktualizacja ścieżki
                            N.Link = Route;

                        }
                    }
                                        
                    if (dbKatalog != null)
                    {
                        dbKatalog = K;
                        db.SaveChanges();
                    }
                    else
                    {
                        db.Katalog.Add(K);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException.InnerException.Message != null)
                    MessageBox.Show("Wystąpił bład podczas dodawania dodatku! :(  " + ex.InnerException.InnerException.Message);
                else
                    MessageBox.Show("Wystąpił bład podczas dodawania dodatku! :(  " + ex);
            }
        }


        public static List<string> GetListaKategorii()
        {
            using (var db = new JMDB())
            {
                //kategorie
                var lista = (from K in db.Kategorie
                              orderby K.Nazwa
                              select K).ToList();
                return lista.Select(n => n.Nazwa).ToList();                
            }
        }

        public static List<string> GetListaDodatkow()
        {
            using (var db = new JMDB())
            {
                //dodatki
                var lista2 = (from K in db.Dodatki
                              orderby K.Nazwa
                              select K).ToList();
                return lista2.Select(n => n.Nazwa).ToList();
            }
        }

        public static Dodatki GetDodatekById(int id)
        {
            Dodatki dodatek = new Dodatki();

            using (var db = new JMDB())
            {
                dodatek = (from D in db.Dodatki                              
                              where
                                D.Ref == id                                
                              select D).FirstOrDefault();             
            }
            return dodatek;
        }

        public static Dodatki GetDodatekByName(string id)
        {
            Dodatki dodatek = new Dodatki();

            using (var db = new JMDB())
            {
                dodatek = (from D in db.Dodatki
                           where
                             D.Nazwa == id
                           select D).FirstOrDefault();
            }
            return dodatek;
        }

        public static List<Dodatki> GetDodatekByName(List<string> id)
        {
            List<Dodatki> dodatki = new List<Dodatki>();

            using (var db = new JMDB())
            {
                foreach (var A in id)
                {
                    var dodatek = (from D in db.Dodatki
                                   where
                                   D.Nazwa == A
                                   select D).FirstOrDefault();
                    dodatki.Add(dodatek);
                }
            }
            return dodatki;
        }

        public static Kategorie GetKategoriaByName(string id)
        {
            Kategorie kategoria = new Kategorie();

            using (var db = new JMDB())
            {
                kategoria = db.Kategorie.FirstOrDefault(x => x.Nazwa == id);                    
            }
            return kategoria;
        }

        public static katalog_kategorie GetKategoriaByNameKatalog(string id)
        {
            katalog_kategorie kategoria = new katalog_kategorie();

            using (var db = new JMDB())
            {

                kategoria = (from K in db.katalog_kategorie
                             where
                             K.Nazwa == id
                             select K).FirstOrDefault();
            }
            return kategoria;
        }

        public static Katalog GetKatalogById(int id)
        {
            List<katalog_zdjecia> zdjecia;
            Katalog katalog;
            using (var db = new JMDB())
            {

                katalog = (from K in db.Katalog
                              where
                              K.Ref == id
                              select K).FirstOrDefault();
                zdjecia = (from K in db.katalog_zdjecia
                           where
                           K.Katalog == katalog.Ref
                           select K).ToList();


                katalog.katalog_zdjecia.Clear();
                foreach (var A in zdjecia)
                    katalog.katalog_zdjecia.Add(A);
            }

            return katalog;
        }

        public static Meble GetMebelById(int id)
        {
            Meble mebelek = new Meble();

            using (var db = new JMDB())
            {
                //pobieranie mebla
                var dbMebel = (from M in db.Meble
                               where M.Ref == id
                               select M).FirstOrDefault();
                //pobieranie zdjęć
                var dbZdjecia = (from Z in db.ZdjeciaMebli
                                 where Z.Mebel == dbMebel.Ref
                                 select Z).ToList();
                //cech
                var dbDodatki = (from D in db.Dodatki
                                 where D.Meble.Select(x => x.Ref).Contains(dbMebel.Ref)
                                 select D).ToList();

                if (dbMebel != null)
                {
                    // ładowanie zdjęć
                    foreach (var A in dbZdjecia)
                    {
                        mebelek.ZdjeciaMebli.Add(A);
                    }
                    if (mebelek.ZdjeciaMebli.Count == 0)
                    {
                        mebelek.ZdjeciaMebli.Add(new ZdjeciaMebli { Link = "Resources/Images/Photos/1.jpg", Numer = 1 });
                    }

                    //ładowanie dodatków
                    foreach (var B in dbDodatki.OrderBy(A => A.Nazwa).ToList())
                    {
                        mebelek.Dodatki.Add(B);
                    }

                    //ładowanie reszty
                    mebelek.Kategorie = dbMebel.Kategorie; //Lategproa
                    mebelek.Nazwa = dbMebel.Nazwa; //Nazwa
                    mebelek.Opis = dbMebel.Opis; // Opis
                    mebelek.OpisKrotki = dbMebel.OpisKrotki; //Opis krótki
                    mebelek.Ref = dbMebel.Ref; //Ref
                    mebelek.Wymiary = dbMebel.Wymiary; //Wymiary
                    mebelek.Kategoria = dbMebel.Kategoria; //Kategoria ID
                }
            }

            return mebelek;
        }
    }
}