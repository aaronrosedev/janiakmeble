﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMS.DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class JMDB : DbContext
    {
        public JMDB()
            : base("name=JMDB")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Dodatki> Dodatki { get; set; }
        public virtual DbSet<Errrors> Errrors { get; set; }
        public virtual DbSet<Kategorie> Kategorie { get; set; }
        public virtual DbSet<StronyTematyczne> StronyTematyczne { get; set; }
        public virtual DbSet<Zdjecia> Zdjecia { get; set; }
        public virtual DbSet<ZdjeciaMebli> ZdjeciaMebli { get; set; }
        public virtual DbSet<Katalog> Katalog { get; set; }
        public virtual DbSet<katalog_kategorie> katalog_kategorie { get; set; }
        public virtual DbSet<katalog_zdjecia> katalog_zdjecia { get; set; }
        public virtual DbSet<Meble> Meble { get; set; }
    }
}
