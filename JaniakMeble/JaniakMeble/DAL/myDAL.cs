﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JaniakMeble.DAL
{
    public class myDAL
    {
        public static Meble GetMebelByName(string id)
        {
            Meble mebelek = new Meble();

            using (var db = new janiakmeble_6Entities())
            {
                //pobieranie mebla
                var dbMebel = (from M in db.Meble 
                               where M.Nazwa == id
                               select  M).FirstOrDefault();
                //pobieranie zdjęć
                var dbZdjecia = (from Z in db.ZdjeciaMebli
                                 where  Z.Mebel == dbMebel.Ref
                                 select Z).ToList();
                //cech
                var dbDodatki = (from D in db.Dodatki                                 
                                 where D.Meble.Select(x => x.Ref).Contains(dbMebel.Ref)
                                 select D).ToList();

                if (dbMebel != null)
                {
                    // ładowanie zdjęć
                    foreach (var A in dbZdjecia)
                    {
                        mebelek.ZdjeciaMebli.Add(A);
                    }
                    if (mebelek.ZdjeciaMebli.Count == 0)
                    {
                        mebelek.ZdjeciaMebli.Add(new ZdjeciaMebli {Link="Resources/Images/Photos/1.jpg", Numer = 1});
                    }

                    //ładowanie dodatków
                    foreach (var B in dbDodatki.OrderBy(A => A.Nazwa).ToList())
                    {
                        mebelek.Dodatki.Add(B);
                    }
         
                    //ładowanie reszty
                    mebelek.Kategorie = dbMebel.Kategorie; //Lategproa
                    mebelek.Nazwa = dbMebel.Nazwa; //Nazwa
                    mebelek.Opis = dbMebel.Opis; // Opis
                    mebelek.OpisKrotki = dbMebel.OpisKrotki; //Opis krótki
                    mebelek.Ref = dbMebel.Ref; //Ref
                    mebelek.Wymiary = dbMebel.Wymiary; //Wymiary
                    mebelek.Kategoria = dbMebel.Kategoria; //Kategoria ID
                }
            }

            return mebelek;
        }

        public static List<Meble> GetMebleWKategorii(string id)
        {
            List<Meble> mebelki = new List<Meble>();
            using (var db = new janiakmeble_6Entities())
            {
                var dbLoad = (from M in db.Meble
                              //from 
                             join K in db.Kategorie on M.Kategoria equals K.Ref
                             where K.Nazwa == id
                             orderby M.Nazwa
                             select M).ToList();
                foreach (var A in dbLoad)
                {
                    Meble B = new Meble();
                    // Ręczne ładowanie danych do zmiennej
                    B.Nazwa = A.Nazwa;
                    B.OpisKrotki = A.OpisKrotki;
                    B.Kategorie = A.Kategorie;
                    B.Ref = A.Ref;
                    B.Wymiary = A.Wymiary;
                    B.ZdjeciaMebli.Add(A.ZdjeciaMebli.OrderBy(n => n.Numer).First());
                    //TODO zdjecie deflautowe
                    mebelki.Add(B);  
                }

            }
            
            return mebelki;
        }

        public static List<Katalog> GetAllCatalogs()
        {
            List<Katalog> listaKatalogow = new List<Katalog>();
            using (var db = new janiakmeble_6Entities())
            {
                var listaKatalogowDB = (from K in db.Katalog
                                       select K).ToList();
                foreach(var A in listaKatalogowDB)
                {
                    var zdjeciaTkaninDB = (from Z in db.katalog_zdjecia
                                           where Z.Katalog == A.Ref
                                           select Z).ToList();

                    var kategoriaKatalogDB = (from K in db.katalog_kategorie
                                              where K.Id == A.Kategoria
                                              select K).First();

                    foreach (var B in zdjeciaTkaninDB)
                        A.katalog_zdjecia.Add(B);
   
                    A.katalog_kategorie = kategoriaKatalogDB;
                    listaKatalogow.Add(A);
                        
                }

                return listaKatalogow;
             }
        }

        public static Katalog GetCatalog(string id)
        {
            Katalog katalog = new Katalog();

            using (var db = new janiakmeble_6Entities())
            {
                var katalogDB = (from K in db.Katalog
                                        where K.Nazwa == id
                                        select K).First();
               
                var zdjeciaTkaninDB = (from Z in db.katalog_zdjecia
                                        where Z.Katalog == katalogDB.Ref
                                        select Z).ToList();

                var kategoriaKatalogDB = (from K in db.katalog_kategorie
                                            where K.Id == katalogDB.Kategoria
                                            select K).First();

                foreach (var B in zdjeciaTkaninDB)
                    katalogDB.katalog_zdjecia.Add(B);

                katalogDB.katalog_kategorie = kategoriaKatalogDB;
                katalog = katalogDB;                

                return katalog;
            }
        }
        public static Zdjecia DeflautPhoto()
        {
            using (var db = new janiakmeble_6Entities())
                return db.Zdjecia.First();
        }

        public static bool Validate<T>(T param)
        {
            return false;
        }
    }
}