﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JaniakMeble.Functions
{
    public class Strings
    {
        public static string RemovePolish(string input)
        {
            return input.Replace("ż", "z").Replace("ó", "o").Replace("ą", "a").Replace("ś", "s").Replace("ę", "e").Replace("ź", "z").Replace("ń", "n").Replace("ł", "l").
                Replace("Ż", "Z").Replace("Ó", "O").Replace("Ą", "A").Replace("Ś", "S").Replace("Ę", "E").Replace("Ź", "Z").Replace("Ń", "N").Replace("Ł", "L");
        }

        public static bool Compare(string input1, string input2)
        {
            return (RemovePolish(input1).ToLower().Equals(RemovePolish(input2).ToLower())) ? true : false;
        }
    }
}