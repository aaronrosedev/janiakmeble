﻿using JaniakMeble.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace JaniakMeble.Functions
{
    public class Error
    {
        public static void Universal(string ErrorMessage, RouteData Route)
        {
            string route = "";
            foreach (var a in Route.Values)
            {
                route += "/" + a.ToString();
            }

            using (var db = new janiakmeble_6Entities())
            {
                db.Errrors.Add(new Errrors() { ErrorRoute = route, ErrorMessage = ErrorMessage });
                db.SaveChanges();
            }
        }
        
    }
}