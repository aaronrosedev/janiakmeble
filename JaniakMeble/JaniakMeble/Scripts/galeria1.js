﻿
var list = document.getElementById('zdjeciaPozostale').querySelectorAll('img');
var zdjecieGlowne = document.getElementById('zdjecieGlowne').querySelectorAll('img')[0];
var showPhoto = document.getElementById('showPhoto');
var showPhotoBackground = document.getElementById('showPhotoBackround');
var leftArrow = document.getElementById('galery_arrow_left');
var rightArrow = document.getElementById('galery_arrow_right');
var bigPhoto = 0;


function arrayObjectIndexOf(myArray, searchTerm) {
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i] === searchTerm) return i;
    }
    return -1;
}

function prevPhoto() {
    if (bigPhoto > 0) {
        bigPhoto--;
        zdjecieGlowne.src = list[bigPhoto].src;
    }
}

function nextPhoto() {
    if (bigPhoto < list.length - 1) {
        bigPhoto++;
        zdjecieGlowne.src = list[bigPhoto].src;
    }
}

function showPhotoAction(url) {
    showPhotoBackground.style.display = 'block'
    var cssImageLoc = "url('" + url +"')"
    showPhoto.style.backgroundImage = cssImageLoc;

    var img = new Image;
    img.src = url;
    var bgImgWidth = img.width;
    var bgImgHeight = img.height;
    showPhoto.style.width = bgImgWidth + 'px';
    showPhoto.style.height = bgImgHeight + 'px';
    showPhoto.style.left = '50%';
    showPhoto.style.left = -bgImgWidth / 2;
    console.log(bgImgWidth + " " + bgImgHeight);
}

function hiddePhotoShower(){
    showPhotoBackground.style.display = 'none';
}



function checkKey(e) {

    e = e || window.event;
       
    if (e.keyCode == '37') {
        prevPhoto();
    }
    else if (e.keyCode == '39') {
        nextPhoto();
    }

}

document.onkeydown = checkKey;



leftArrow.onclick = function () {
    prevPhoto();
}

rightArrow.onclick = function () {
    nextPhoto();   
}

zdjecieGlowne.onclick = function () {
    showPhotoAction(zdjecieGlowne.src);
}

showPhotoBackground.onclick = function () {
    hiddePhotoShower();
}

 onclick="showPhoto.style.display = 'none'" 

//document.not(showPhoto).onclick() = function () {
//    showPhoto.style.display = 'none';
//}


for (i = 0, len = list.length; i < len; i++) {
    list[i].onclick = function () {
        zdjecieGlowne.src = this.src;
        bigPhoto = arrayObjectIndexOf(list, this);        
    };
}
