﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JaniakMeble.Models
{
    public class Zdjecie
    {
        [Key]
        public int ID { get; set; }
        public string link { get; set; }
        public List<Tag> tagi { get; set; }
        public Zdjecie()
        {
            tagi = new List<Tag>();
        }
    }
}