﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JaniakMeble.Models
{
    public class Dodatki
    {
        [Key]
        public int ID { get; set; }
        public string nazwa { get; set; }
        public string opis { get; set; }
        public Zdjecie zejecie { get; set; }
        public List<Tag> tagi { get; set; }
        public Dodatki()
        {
            tagi = new List<Tag>();
        }

    }
}