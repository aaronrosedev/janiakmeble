﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JaniakMeble.Models
{
    public class Kategoria
    {
        [Key]
        public int ID { get; set; }
        public string nazwa { get; set; }
        public string link { get; set; }
        public Zdjecie zdjecie { get; set; }
        public List<Tag> tagi { get; set; }
        public Kategoria()
        {
            tagi = new List<Tag>();
        }

    }
}