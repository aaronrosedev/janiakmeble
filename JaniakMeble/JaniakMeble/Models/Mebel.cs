﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JaniakMeble.Models
{
    public class Mebel
    {
        [Key]
        public int ID { get; set; }
        public string nazwa { get; set; }
        public string opis { get; set; }
        public string wymiary { get; set; }
        public List<Zdjecie> zdjecia { get; set; }
        public List<Dodatki> dodatki { get; set; }
        public List<Tag> tagi { get; set; }
        public Mebel()
        {
            zdjecia = new List<Zdjecie>();
            dodatki = new List<Dodatki>();
            tagi = new List<Tag>();

        }
        
    }
}