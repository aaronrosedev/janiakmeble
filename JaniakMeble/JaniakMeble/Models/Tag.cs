﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JaniakMeble.Models
{
    public class Tag
    {
        [Key]
        public int ID { get; set; }
        public string tag { get; set; }
    }
}