﻿using JaniakMeble.DAL;
using JaniakMeble.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JaniakMeble.Controllers
{
    public class OfferController : Controller
    {
        //
        // GET: /Offer/
        public ActionResult Index()
        {
            ViewBag.Title = "Kategorie";
            ViewBag.Route = "Oferta -> Kategorie";

            List<Kategorie> kategorie = new List<Kategorie>();
            using (var A = new janiakmeble_6Entities())
            {
                var B = A.Kategorie;
                foreach (var C in B)
                {
                    kategorie.Add(C);
                }
            }
            return View(kategorie.OrderBy(x => x.Nazwa));
        }

        public ActionResult Category(string id)
        {
            List<Meble> mebelki = new List<Meble>();

            mebelki = myDAL.GetMebleWKategorii(id);

           
            try
            {
                ViewBag.Title = mebelki.Select(A => A).First().Kategorie.Nazwa;
            }
            catch
            {
                Error.Universal("KuPa", RouteData);
                return View("PageError");
            }
            return View(mebelki);
        }

        public ActionResult Product(string id)
        {
            ViewBag.Title = "Nazwa produktu";
            ViewBag.Route = "Oferta -> Kategorie -> Nazwa Produktu";
            Meble mebelek = new Meble();
            mebelek = myDAL.GetMebelByName(id);


            ViewBag.Title = mebelek.Nazwa;
            
            return View(mebelek);
        }

        public ActionResult ProductGalery(string id)
        {
            Meble mebelek = new Meble();
            mebelek = myDAL.GetMebelByName(id);

            return View(mebelek);
        }
	}
}