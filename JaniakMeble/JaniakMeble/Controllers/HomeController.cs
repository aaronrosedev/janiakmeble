﻿using JaniakMeble.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JaniakMeble.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        //public ActionResult Index()
        //{           
        //    StronyTematyczne str2 = new StronyTematyczne();
        //    using (var db = new janiakmeble_6Entities())
        //    {
        //        var strona = from str in db.StronyTematyczne
        //                     where str.nazwa == "Strona główna"
        //                     select str;
        //        foreach (var a in strona)
        //        {
        //            str2 = a;
        //        }
        //        ViewBag.Title = str2.nazwa;
        //        ViewBag.Content = str2.tekstHTML.Replace("~","..");
        //        ViewBag.Route = "Home";

        //        return View();
        //    }
        //}

        public ActionResult Index(string id)
        {
            if (id == null) id = "Strona główna";
            StronyTematyczne str2 = new StronyTematyczne();
            using (var db = new janiakmeble_6Entities())
            {
                var strona = from str in db.StronyTematyczne
                             where str.nazwa == id
                             select str;
                foreach (var a in strona)
                {
                    str2 = a;
                }
                ViewBag.Title = str2.nazwa;
                ViewBag.Content = str2.tekstHTML.Replace("~", "..");
                ViewBag.Route = "Home -> " + str2.nazwa;

                return View();
            }
        }
        
        public ActionResult Dodatki()
        {
            List<Dodatki> dodatki;
            using (var db = new janiakmeble_6Entities())
            {
                dodatki = (from D in db.Dodatki
                          orderby D.Nazwa
                          select D).OrderBy(n=>n.Nazwa).ToList();                
            }
            return View(dodatki);
        }
       
	}
}