﻿using JaniakMeble.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JaniakMeble.Controllers
{
    public class CatalogController : Controller
    {
        // GET: Catalog
        public ActionResult Index()
        {

            var listaKatalogow = myDAL.GetAllCatalogs();
            return View(listaKatalogow);
        }


        public ActionResult Details(string id)
        {
            var katalog = myDAL.GetCatalog(id);
            return View(katalog);
        }

    }
}